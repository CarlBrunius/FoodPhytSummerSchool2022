# FoodPhyt Summer School 2022
Welcome to the repository for the Data Analysis section of the FoodPhyt Summer School 2022

Associate Professor Carl Brunius <carl.brunius@chalmers.se>  
Department of Biology and Biological Engineering  
Chalmers University of Technology  

# General info
This repository contains course documents and an R package for data analysis  
- Course documents (lecture slides, instructions and R scripts) can be found under the `Course Documents` folder in the repository
- The R library is installed using instructions found under `Course Documents/Scripts/0_setup.R` OR by following the instructions below

# Installation of "everything"
After following these instructions you should have your computer completely set up for doing the computer lab!  
You will install R, RStudio and some packages, including `remotes`, `FoodPhytSummerSchool2022`, `rdCV`, `pls`, `BiocManager` and `mixOmics`.  

- First download and install R from https://www.r-project.org/
- Then download and install RStudio from https://rstudio.com/

To install the relevant packages, you need to work in R. So go ahead and start RStudio. You will need to have the `remotes` R package installed. Just run the following from an R script or type it directly at the R console (normally the lower left window in RStudio):
```
install.packages('remotes')
```
When `remotes` is installed, you can simply install the `FoodPhytSummerSchool2022` package like this:
```
library(remotes)
install_gitlab('CarlBrunius/FoodPhytSummerSchool2022')
```
You will also need some other R packages. Just copy the code chunk below and run it from a script or directly in your R console. Please note that R may ask you to update packages to later versions. This is normally a good idea. Update all packages when prompted. However, R will sometimes tell you that there are binary versions available but that source versions are later. I normally avoid installing from source to avoid problems that sometimes occur when needing to compile from source. Just say no.
```
install.packages("BiocManager")
BiocManager::install("mixOmics")
install.packages('pls')
install_gitlab('CarlBrunius/rdCV')
install_gitlab('CarlBrunius/MUVR')
```
Now you should be all set!

# Time to start working
I suggest to start the actual work by reading __"A Super Brief R Tutorial"__ in the `Course Documents` folder.  
The is a study document called __"Data Analysis Computer Lab"__, also in the `Course Documents` folder. This document was adapted from another teaching package of mine and may not be complete - however, it anyway contains some useful information!

Happy coding!

Carl


# Version history
version | date | comment
:------ | :--- | :------
0.1.0 | 2022-10-13 | 1st commit


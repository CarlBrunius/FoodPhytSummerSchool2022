#############################################################
#############################################################
## In the following task we'll return to random data 
## and investigate the consequence of different 
## cross-validation schemes
##
#############################################################
## Hard-setting nComp to 2
## Single cross-validation for determining nComp (like standard software)
## Single cross-validation with holdout predictions
## Repeated double cross-validation 
## MUVR
#############################################################



# clean slate
rm(list = ls())

# set-up
nGroup <- 2
nSampPerGroup <- 25
nSamples <- nSampPerGroup * nGroup
nVar <- 100

# Make random groups
groups <- makeGroups(nGroup = nGroup, nSampPerGroup = nSampPerGroup)
head(groups)
tail(groups)
View(groups)

# make random data
# Make many random variables by adapting the code above
randomData <- makeRandomMatrix(nSamp = nSamples,
                               nVar = nVar,
                               sampNames = groups$sample,
                               maxInt = 100) # Make a random matrix with 50 samples and 1000 variable ranging from 0 to 100 in a uniform distribution


# Take a look at the random data
randomData[1:10, 1:5]
dim(randomData)


#############################################################
#############################################################
# Random conditions 
# There should be no associations
# Between X and Y
# 
# Let's test this using permutations
#############################################################


#############################################################
# Establish a reference state using only random draws
# With no modelling!
#############################################################

nPerm <- 100000
CR_random <- numeric(nPerm) # store permutation classification rates

# Actual permutations
for (p in 1:nPerm) {
  permGroup <- sample(groups$group, replace = TRUE) # Make a random draw
  CR_random[p] <- sum(groups$group == permGroup) # Number of correct guesses
}
CR_random <- CR_random / nSamples # Convert to classification rate

hist(CR_random, xlim = c(0,1), col = rgb(1, 0, 0, .5), probability = TRUE, breaks = 40)
abline(v = 0.5, col = 'grey', lty = 2)
mean(CR_random) # Approximately 0.5; corresponds to the intuition
median(CR_random) # Exactly 0.5; corresponds to the intuition



#############################################################
# PLS modelling
# A whole lot of overfitting!
#############################################################

library(pls)
library(FoodPhytSummerSchool2022)

plsda <- plsDA(X = randomData,
               Y = groups$group,
               nComp = 2)
plsda$table
plotPLSDA(plsda)

# Do many permutations
nPerm <- 1000
CR_pls <- numeric(nPerm) # store permutation classification rates

for (p in 1:nPerm) {
  permGroup <- sample(groups$group, replace = TRUE) # Make a random draw
  plsPerm <- plsDA(X = randomData, # model the random group membership
                   Y = permGroup,
                   nComp = 2)
  CR_pls[p] <- sum(plsPerm$predicted == permGroup) # Number of correct predictions - should be around 0.5 under random conditions!
}
CR_pls <- CR_pls / nSamples # Convert to classification rate

par(mfrow = c(2, 1))
hist(CR_random, xlim = c(0,1), col = rgb(1, 0, 0, .5), probability = TRUE, breaks = 40)
abline(v = 0.5, col = 'grey', lty = 2)
hist(CR_pls, xlim = c(0,1), col = rgb(0, 1, 0, 0.5), probability = TRUE) # Histogram of random classification rate
abline(v = 0.5, col = 'grey', lty = 2)
mean(CR_pls) # close to 1 - Severe overfitting
median(CR_pls) # 1 - Severe overfitting



#######################################
## Single cross-validation - Two types

pls_CV <- CVplsDA(X = randomData,
                  Y = groups$group)

# Look at the classifications
# CV_Fit
pls_CV$table_fit
# CV_Holdout
pls_CV$table_CV

# Do many permutations
nPerm <- 100
CR_pls_CV_Fit <- numeric(nPerm) # store permutation classification rates
CR_pls_CV_Holdout <- CR_pls_CV_Fit

for (p in 1:nPerm) {
  permGroup <- sample(groups$group, replace = TRUE) # Make a random draw
  plsPerm <- CVplsDA(X = randomData, # model the random group membership
                     Y = permGroup)
  CR_pls_CV_Fit[p] <- sum(plsPerm$predict_fit == permGroup) # Number of correct predictions - should be around 0.5 under random conditions!
  CR_pls_CV_Holdout[p] <- sum(plsPerm$predict_CV == permGroup) # Number of correct predictions - should be around 0.5 under random conditions!
}
CR_pls_CV_Fit <- CR_pls_CV_Fit / nSamples # Convert to classification rate
CR_pls_CV_Holdout <- CR_pls_CV_Holdout / nSamples # Convert to classification rate

par(mfrow = c(4, 1))
hist(CR_random, xlim = c(0,1), col = rgb(1, 0, 0, .5), probability = TRUE, breaks = 40)
abline(v = 0.5, col = 'grey', lty = 2)
hist(CR_pls, xlim = c(0,1), col = rgb(0, 1, 0, 0.5), probability = TRUE) # Histogram of random classification rate
abline(v = 0.5, col = 'grey', lty = 2)
hist(CR_pls_CV_Fit, xlim = c(0,1), col = rgb(1, 1, 0, 0.5), probability = TRUE) # Histogram of random classification rate
abline(v = 0.5, col = 'grey', lty = 2)
hist(CR_pls_CV_Holdout, xlim = c(0,1), col = rgb(0, 1, 1, 0.5), probability = TRUE) # Histogram of random classification rate
abline(v = 0.5, col = 'grey', lty = 2)
mean(CR_pls_CV_Fit) # close to 1 - Severe overfitting
median(CR_pls_CV_Fit) # 1 - Severe overfitting
mean(CR_pls_CV_Holdout) # close to 0.55 limited overfitting
median(CR_pls_CV_Holdout) # close to 0.55 limited overfitting



########################
# rdCV-PLS modelling
# Virtually no overfitting!

# Set up for parallel processing
library(rdCV) # https://gitlab.com/CarlBrunius/rdCV
library(doParallel)
nCore <- detectCores() - 1
cl <- makeCluster(nCore)
registerDoParallel(cl)

# Make the actual model
rdCVModel <- rdCV(X = randomData,
                  Y = groups$group,
                  nRep = nCore,
                  method = 'PLS',
                  fitness = 'MISS')
stopCluster(cl)

# Look at the classifications
table(actual = groups$group,
      predicted = rdCVModel$yClass)

# Permutations
nPerm <- 30
CR_rdCV <- numeric(nPerm) # store permutation classification rates

cl <- makeCluster(nCore)
registerDoParallel(cl)

for (p in 1:nPerm) {
  permGroup <- sample(groups$group, replace = TRUE) # Make a random draw
  rdCVPerm <- rdCV(X = randomData,
                   Y = permGroup,
                   nRep = nCore,
                   method = 'PLS',
                   fitness = 'MISS')
  CR_rdCV[p] <- sum(rdCVPerm$yClass == permGroup) # Number of correct predictions - should be around 0.5 under random conditions!
}
stopCluster(cl)
CR_rdCV <- CR_rdCV / nSamples # Convert to classification rate

# Look at output from permutations
par(mfrow = c(5, 1))
hist(CR_random, xlim = c(0,1), col = rgb(1, 0, 0, .5), probability = TRUE, breaks = 40)
abline(v = 0.5, col = 'grey', lty = 2)
hist(CR_pls, xlim = c(0,1), col = rgb(0, 1, 0, 0.5), probability = TRUE) # Histogram of random classification rate
abline(v = 0.5, col = 'grey', lty = 2)
hist(CR_pls_CV_Fit, xlim = c(0,1), col = rgb(1, 1, 0, 0.5), probability = TRUE) # Histogram of random classification rate
abline(v = 0.5, col = 'grey', lty = 2)
hist(CR_pls_CV_Holdout, xlim = c(0,1), col = rgb(0, 1, 1, 0.5), probability = TRUE) # Histogram of random classification rate
abline(v = 0.5, col = 'grey', lty = 2)
hist(CR_rdCV, xlim = c(0,1), col = rgb(0, 0, 1, 0.5), probability = TRUE) # Histogram of random classification rate
abline(v = 0.5, col = 'grey', lty = 2)
mean(CR_rdCV) # close to 0.5 - Virtually no overfitting
median(CR_rdCV) # close to 0.5 - Virtually no overfitting

